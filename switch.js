var gpio = require('pi-gpio');

var PinMonitor = function(pin_number, interval, initial_state) {
        this.state = initial_state;
        this.pin_number = pin_number;
        this.interval = interval;
};
PinMonitor.prototype.poll = function() {
	var self = this;
	gpio.read(this.pin_number, function(err, new_state) {
        	if(new_state!=self.state) {
                	self.state = new_state;
                	self.callback(self);
        	}
	});
};
PinMonitor.prototype.start = function(callback) {
        if(callback!=null) this.callback = callback;
        console.log("Starting polling for " + this.pin_number + " every " + this.interval + " milliseconds.");
        var poll = this.poll;
        this.timer = setInterval(this.poll.bind(this), this.interval);
};
PinMonitor.prototype.stop = function() {
        clearInterval(this.timer);
};

gpio.open(36, 'input', function(err) {
	var pin_monitor = new PinMonitor(36, 50);
	pin_monitor.start(function(monitor) {
        	console.log("Pin " + monitor.pin_number + " changed to " + monitor.state);
	});
});

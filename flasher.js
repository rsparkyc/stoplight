var gpio=require('pi-gpio');
var async = require('async');

console.log('Starting Application');

var RED_PIN = 37;
var YELLOW_PIN = 38;
var GREEN_PIN = 40;

var binaryPins = [11, 13, 15, 29, 31, 33, 35, 37]

var initializeOutputPin = function(pin, next) {
	console.log('opening pin ' + pin);
	gpio.open(pin, 'output', next);
}

async.each(binaryPins, initializeOutputPin, function(err) {
	if (err) {
		console.log('ERROR: ' + err);
		return;
	}
	lightNextPin(binaryPins[0]);
});

function lightNextPin(pin) {
	console.log('lightNextPin called for pin ' + pin);
	//turn on pin
	console.log('writing 1 to pin ' + pin);
	gpio.write(pin, 1);

	//turn off pin
	setTimeout(function() {
		console.log('writing 0 to pin ' + pin);
		gpio.write(pin, 0);
	}, 600);

	setTimeout(function() {
		var currentIndex = binaryPins.indexOf(pin);
		var nextIndex = ++currentIndex;
		if (nextIndex == binaryPins.length) {
			nextIndex = 0;
		}

		return lightNextPin(binaryPins[nextIndex]);
	}, 200);
}


process.on('exit', cleanup);
process.on('SIGINT', cleanup);

function cleanup() {
	console.log('closing pins');
	async.each(binaryPins, closePin, function(err) {
		if (err) {
			console.error('problem closing pins: ' + err);
		}
	});
	process.exit();
}

function closePin(pin, next) {
	console.log('closing pin: ' + pin);
	gpio.close(pin);
	next();
}



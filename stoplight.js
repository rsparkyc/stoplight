var gpio=require('pi-gpio');
var async = require('async');

console.log('Starting Application');

var RED_PIN = 37;
var YELLOW_PIN = 38;
var GREEN_PIN = 40;

var initializeOutputPin = function(pin, next) {
	console.log('opening pin ' + pin);
	gpio.open(pin, 'output', next);
}

async.each([RED_PIN, YELLOW_PIN, GREEN_PIN], initializeOutputPin, function(err) {
	if (err) {
		console.log('ERROR: ' + err);
		return;
	}
	setTimeout(function() {
		console.log('writing to ' + RED_PIN);
		gpio.write(RED_PIN, 1, function() {
		console.log('closing ' + RED_PIN);
		gpio.close(RED_PIN); });	
	}, 1000);
	setTimeout(function() {
		console.log('writing to ' + YELLOW_PIN);
		gpio.write(YELLOW_PIN, 1, function() {
		console.log('closing ' + YELLOW_PIN);
		gpio.close(YELLOW_PIN);	 });
	}, 2000);
	setTimeout(function() {
		console.log('writing to ' + GREEN_PIN);
		gpio.write(GREEN_PIN, 1, function() {
		console.log('closing ' + GREEN_PIN);
		gpio.close(GREEN_PIN); });
	}, 3000);
});

